% interfaces = tree.find_all(kind='interface', sort_by='DEFAULT')
% with_impls = [ y for x,y in interfaces if y.implemented_by ]
% if interfaces:
  <h1>Interfaces <span>{{len(with_impls)}} of {{len(interfaces)}} with implementing components</span></h1>
  <dl>
    % for path, interface in interfaces:
      % include interface path=path, interface=interface, detail='full'
    % end
  </dl>
% end

% rebase layout tree=tree
