% if element.references:
  <tr>
    <th>References</th>
    <td>
      <ul class="list">
        % for refname, url in element.references.iteritems():
          <li>
            <a href="{{ url }}">{{ refname }}: ({{ url }})</a>
          </li>
        % end
      </ul>
    </td>
  </tr>
% end
